var pytaniaOdpowiedzi= [
	{
		nazwa: "Pytanie numer 1",
		pytanie: "Dlaczego niebo jest niebieskie?",
		a: "jest przezroczyste, to przez budowę naszego oka jest niebieskie",
		b: "kolor niebieski jest w przyrodzie dominujący",
		c: "powietrze jest tak naprawdę ciekłe",
		d: "żadna z powyższych",
		prawidlowa: "b"
	},
	{
		nazwa: "Pytanie numer 2",
		pytanie: "Co to jest system szesnastkowy",
		a: "zapis liczb od 0 do F",
		b: "zapis liczb od 1 do F",
		c: "operowanie na liczba podzielnych przez 16",
		d: "zamian liczb ujemnych na dodatnie",
		prawidlowa: "a"
	}

]